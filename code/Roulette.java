import java.util.Scanner;
public class Roulette {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int bankAccount = 1000;
        int betAmount;
        int betNumber;
        boolean playing = true;
        //Game in progress
          while(playing) 
          { 
            System.out.println("How much would the player like to bet?");

            betAmount = validateBetAmount(reader.nextInt(), bankAccount);

            System.out.println("What number would you like to bet on? (0-36)");

            betNumber = validateBetNum(reader.nextInt());

            wheel.spin();

            System.out.println("NUMBER SPUN: " + wheel.getValue());

            int winningNum = wheel.getValue();

            bankAccount = winOrLose(betNumber, winningNum, betAmount, bankAccount);

            playing = keepPlaying(bankAccount);
          }
          //Game over
          gameResults(bankAccount);
    }

    //Validates that the bet amount is within the players budget & not 0
    public static int validateBetAmount(int betAmount, int bankAccount){
        Scanner reader = new Scanner(System.in);
        boolean validAnswer = false;
        while(!validAnswer)
        {
            if(betAmount > bankAccount){
                    System.out.println("You don't have that much! Try again.");
                    betAmount = reader.nextInt();
                }
            else if(betAmount <= 0){
                System.out.println("Invalid amount! Try again.");
                betAmount = reader.nextInt();
            }
            else {
                validAnswer = true;
            }
        }
        return betAmount;
    }

    //Validates the number the player decides to bet on
    public static int validateBetNum(int betNumber){
        Scanner reader = new Scanner(System.in);
        while(betNumber < 0 || betNumber > 36){
            System.out.println("That number isn't on the wheel! Try again.");
            betNumber = reader.nextInt();
        }
        return betNumber;
    }

    //Updates the players bank account in the event of a win/lose
    public static int winOrLose(int betNumber, int winningNum, int betAmount, int bankAccount ){
        int newBankAccount = 0;
        if(betNumber == winningNum){
                newBankAccount = bankAccount + (betAmount*35);
                System.out.println("You won! Here's how much money you have:");
                System.out.println(newBankAccount);
                return newBankAccount; 
            }
        else {
                newBankAccount = (bankAccount - betAmount);
                System.out.println("You lost :( Here's how much money you have:");
                System.out.println(newBankAccount);
                  return newBankAccount;  
            }
    }

    //Asks the player if they'd like to keep playing, ends the game if not
    public static boolean keepPlaying(int bankAccount){
        Scanner reader = new Scanner(System.in);
        boolean validAnswer = false;
        boolean replay = false;
        
        if(bankAccount <= 0)
        {
            System.out.println("You have no money left to bet!");
            return replay;
        }
        while(!validAnswer)
        {
            System.out.println("Would you like to play again? (Yes or No)");
            String playing = reader.next();   
            if(playing.equals("No") || playing.equals("no"))
                {
                    replay = false;
                    validAnswer = true;
                }
                else if(playing.equals("Yes") || playing.equals("yes"))
                {
                    replay = true;
                    validAnswer = true;
                }
                else 
                {
                validAnswer = false;  
                }
        }
        return replay;
    }

    //Prints the total amount the player has won/lost
    public static void gameResults(int bankAccount){
        System.out.println("Game Over! Here's your results:");
            if(bankAccount < 1000)
            {
                System.out.println("You lost: " + (1000 - bankAccount));
                System.out.println("You have: " + bankAccount);
            }
            else {
                System.out.println("You won " + (bankAccount - 1000));
                System.out.println("You lost: " + bankAccount);
            }
    }
}