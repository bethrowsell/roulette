import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int lastSpin;

    public RouletteWheel(){
        random = new Random();
        lastSpin = 0;
    }

    public void spin(){
        int newNum = random.nextInt(37);
        lastSpin = newNum;
        
    }

    public int getValue(){
        return lastSpin;
    }
}
